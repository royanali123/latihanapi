<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function route1(){
        return "Berhasil Masuk Route-1";
    }

    public function route2(){
        return "Berhasil Masuk Route-2";
    }

    public function route3(){
        return "Berhasil Masuk Route-3";
    }
}
