<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'admin'])->group(function () {
    //
    Route::get('/route-1',"AdminController@route1")->name("route1");
    Route::get('/route-2',"AdminController@route2")->name("route2");
    Route::get('/route-3',"AdminController@route3")->name("route3");
});


