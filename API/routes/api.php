<?php

Route::namespace("Auth")->group(function(){
    Route::post('register','RegisterController');
    Route::post('login','LoginController');
    Route::post('logout','LogoutController');

});
Route::get('user',"UserController")->middleware("auth:api");
Route::namespace("Article")->middleware("auth:api")->group(function(){
    Route::post('create-new-article','ArticleController@store');
    Route::put('update-the-selected-article/{article}','ArticleController@update');
    Route::delete('delete-the-selected-article/{article}','ArticleController@destroy');
});
Route::get("article/{article}","Article\ArticleController@show");
Route::get("article/","Article\ArticleController@index");

