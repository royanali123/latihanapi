<?php

namespace App\Models\Article;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $guarded = [];
    protected $with = ["subject", "user"];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function subject(){
        return $this->belongsTo(Subject::class);
    }
    public function getRouteKeyName() //Memberi Slug pada Route
    {
        return "slug";
    }
}
