<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Models\Article\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\User;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::paginate(1);
        return new ArticleCollection($article);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "title" => ["required", "min:3", "max:255"],
            "body" => "required",
            "subject" => "required"
        ]);
        // dd( User::find(Auth::id())->articles());
        $user = User::find(Auth::id());
        // dd($user);
        $value = $user->article()->create([
            "title" => $request["title"],
            "body" => $request["body"],
            "subject_id" => $request["subject"],
            "slug" => Str::slug($request["title"])
        ]);
        return $value;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article) // Meendapatkan data Artikel
    {
        return new ArticleResource($article); // Cara menampilkan dengan Resource
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        $articles = $article->update([
            "title" => $request["title"],
            "body" => $request["body"],
            "subject_id" => $request["subject"],
            "slug" => Str::slug($request["title"])
        ]);
        return new ArticleResource($article);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $article->delete();
        return response()->json("Delete Done!");
    }
}
