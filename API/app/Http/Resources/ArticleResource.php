<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) // Data yang diTampilkan
    {
        // data => Edit di AppServiceProvider Register menggunakan JsonResource::warp("items")
        return [
            "title" => $this->title,
            "publihed" => $this->created_at->diffForHumans(),
            "subject" => $this->subject,
            "author" => $this->user->name
            // format('d F Y')
        ];
    }
    public function with($request)
    {
        return ["status" => "success"];
    }
}
