<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Cara 1
Route::get("/krs","KrsController@krs")->middleware('krs');
Route::get("/krs1","KrsController@krs1");
//Cara 2
// Route::group(['middleware' => ['krs']], function () {
//     Route::get("/krs","KrsController@krs");
// });

// Cara 3 di controller beri middleware
// Route::get("/krs","KrsController@krs");
// Route::get("/krs1","KrsController@krs1");
