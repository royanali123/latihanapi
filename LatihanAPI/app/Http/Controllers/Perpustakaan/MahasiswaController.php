<?php

namespace App\Http\Controllers\Perpustakaan;

use App\Http\Controllers\Controller;
use App\Http\Requests\MahasiswaRequest;
use App\Mahasiswa;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MahasiswaController extends Controller
{

    public function store(MahasiswaRequest $request){
        $user = User::find(Auth::id());
        // dd($user->mahasiswa);
        if ($user->mahasiswa == null){
            $user->mahasiswa()->create([
                "name" => $request["name"],
                "nim" => $request["nim"],
                "fakultas" => $request["fakultas"],
                'no_hp' => $request["no_hp"],
                "no_wa" => $request["no_wa"]
            ]);
        }
        return response("You Already Exits");

    }
}
