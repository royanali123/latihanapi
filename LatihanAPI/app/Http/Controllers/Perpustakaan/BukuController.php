<?php

namespace App\Http\Controllers\Perpustakaan;

use App\Buku;
use App\Http\Controllers\Controller;
use App\Http\Requests\BukuRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class BukuController extends Controller
{

    public function store(BukuRequest $request)
    {
        Buku::create([
            "kode_buku" => $request["kode_buku"],
            "judul" => $request["judul"],
            "pengarang" => $request["pengarang"],
            'tahun_terbit' => $request["tahun_terbit"],
        ]);
    }
}
