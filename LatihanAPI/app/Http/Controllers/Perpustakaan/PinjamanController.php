<?php

namespace App\Http\Controllers\Perpustakaan;

use App\Http\Controllers\Controller;
use App\Http\Requests\PinjamanRequest;
use App\Pinjaman;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PinjamanController extends Controller
{
    public function store(PinjamanRequest $request)
    {

        $user = User::find(Auth::id());
        if ($user->pinjam->isEmpty()){
            $user->pinjam()->create([
                "tanggal_pinjaman" => $request["tgl_pinjaman"],
                "tanggal_batas_pinjaman" => $request["tgl_batas_pinjaman"]
            ]);
        }
        return response("You Already Exits");

    }
    public function update(Request $request,Pinjaman $pinjaman){
        // return $pinjaman->id;
        $request->validate([
            "tgl_pengembalian" => ["date","required"],
            "status" => "required"
        ]);
        Pinjaman::where("id",$pinjaman["id"])->update([
            "tanggal_pengembalian" => $request["tgl_pengembalian"],
            "status_ontime" => $request["status"]
        ]);
        return "Done";
    }
}
