<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrasiRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class RegistrasiController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegistrasiRequest $request)
    {
       User::create([
            "username" => $request["username"],
            "email" => $request["email"],
            "name" => $request["name"],
            "password" => bcrypt($request["password"])
       ]);

       return response("Thank You For Registrasi!!");
    }
}
