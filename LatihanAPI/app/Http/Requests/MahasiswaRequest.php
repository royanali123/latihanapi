<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MahasiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required","string","min:3","max:255"],
            "nim" => ["required","alpha_num",'min:9',"unique:mahasiswas,nim"],
            "fakultas" => "required",
            'no_hp' => ["required","regex:/(08)[0-9]{10}/"],
            "no_wa" => ["required","regex:/(08)[0-9]{10}/"],
        ];
    }
}
