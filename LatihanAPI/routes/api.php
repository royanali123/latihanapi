<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace("Auth")->group(function(){
    Route::post('registrasi',"RegistrasiController");
    Route::post('login',"LoginController");
    Route::post('logout',"LogoutController");
});

Route::namespace("Perpustakaan")->middleware("auth:api")->group(function(){
    Route::post('data-mahasiswa',"MahasiswaController@store");
    Route::post('data-buku',"BukuController@store");
    Route::post('data-pinjaman',"PinjamanController@store");
    Route::patch('data-pengembalian/{pinjaman}',"PinjamanController@update")->name("pengembalian")->middleware("admin");

});

Route::get('user',"UserController@index")->middleware("auth:api");
